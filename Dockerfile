ARG NGINX_VERSION="1.18.0"
ARG CRS_VERSION="3.3.0"

FROM nginx:${NGINX_VERSION}-alpine as build

ARG NGINX_VERSION
ARG CRS_VERSION

RUN apk add --update --no-cache alpine-sdk apr-dev apr-util-dev autoconf automake binutils-gold curl curl-dev g++ gcc geoip-dev git gnupg icu-dev libcurl libffi-dev libjpeg-turbo-dev libstdc++ libtool libxml2-dev linux-headers lmdb-dev m4 make openssh-client pcre-dev pcre2-dev perl pkgconf wget yajl-dev zlib-dev \
 && mkdir -p /src \
 && git clone --depth 1 -b v3.0.4 --single-branch https://github.com/SpiderLabs/ModSecurity /src/ModSecurity \
 && git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git /src/ModSecurity-nginx \
 && wget -qO - https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz | tar xzf - -C /src \
 && wget -qO - https://github.com/coreruleset/coreruleset/archive/v${CRS_VERSION}.tar.gz | tar xzf - -C /src \
 && cd /src/ModSecurity \
 && git submodule init \
 && git submodule update \
 && ./build.sh \
 && ./configure \
 && make \
 && make install \
 && cd /src/nginx-${NGINX_VERSION} \
 && ./configure --with-compat --add-dynamic-module=../ModSecurity-nginx \
 && make modules \
 && find /usr/local -name "*.a" -print | xargs /bin/rm

#############################################################################################################################################

FROM nginx:${NGINX_VERSION}-alpine

ARG CRS_VERSION

RUN apk add --update --no-cache libcurl yajl libstdc++

COPY --from=build /usr/local/modsecurity /usr/local/modsecurity
COPY --from=build /src/nginx-${NGINX_VERSION}/objs/ngx_http_modsecurity_module.so /etc/nginx/modules
COPY --from=build /src/ModSecurity/modsecurity.conf-recommended /etc/nginx/modsecurity/modsecurity.conf
COPY --from=build /src/ModSecurity/unicode.mapping /etc/nginx/modsecurity/unicode.mapping
COPY --from=build /src/coreruleset-${CRS_VERSION}/crs-setup.conf.example /etc/nginx/modsecurity/crs/crs-setup.conf
COPY --from=build /src/coreruleset-${CRS_VERSION}/rules /etc/nginx/modsecurity/crs/rules

RUN echo -e "load_module modules/ngx_http_modsecurity_module.so;\n$(cat /etc/nginx/nginx.conf)" > /etc/nginx/nginx.conf \
 && echo "Include /etc/nginx/modsecurity/crs/crs-setup.conf" >> /etc/nginx/modsecurity/main.conf \
 && echo "Include /etc/nginx/modsecurity/crs/rules/*.conf" >> /etc/nginx/modsecurity/main.conf \
 && echo "Include /etc/nginx/modsecurity/main.conf" >> /etc/nginx/modsecurity/modsecurity.conf \
 && sed -i 's/SecRuleEngine DetectionOnly/SecRuleEngine On/' /etc/nginx/modsecurity/modsecurity.conf \
 && sed -i 's/^SecAuditLog .*$/SecAuditLog \/dev\/stdout/g' etc/nginx/modsecurity/modsecurity.conf \
 && sed -i 's/^SecAuditLogParts .*$/SecAuditLogParts AHZ/g' etc/nginx/modsecurity/modsecurity.conf
